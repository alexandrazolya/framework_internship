/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       IoHwAb.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the (non-standardized by AUTOSAR) I/O hardware abstraction interfaces for accessing and
 *                controlling all the sensor and actuator ECU signals through direct access to the MCAL drivers.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "IoHwAb.h"

#if (STD_ON == IOHWAB_API)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------ Analog Interfaces ------------------------------------------------*/

#if (STD_ON == IOHWAB_ANALOG_API)

/**
 * \brief      Performs a synchronous read of the specified MCAL ADC channel.
 * \param      ChannelId: ID of a specific MCAL ADC channel.
 * \return     Result of the internal MCAL ADC conversion for the specified channel.
 */
IoHwAb_AnalogLevelType IoHwAb_AnalogGetChannel(IoHwAb_AnalogChannelType ChannelId)
{
   IoHwAb_AnalogLevelType t_Return;
   Adc_SetupResultBuffer((Adc_GroupType) ChannelId, (Adc_ValueGroupType *) &t_Return);
   Adc_StartGroupConversion((Adc_GroupType) ChannelId);
   while (ADC_BUSY == Adc_GetGroupStatus((Adc_GroupType) ChannelId))
   {
      /* Wait for the conversion to finish. */
   }
   return t_Return;
}

#endif /* (STD_ON == IOHWAB_ANALOG_API) */

/*------------------------------------------------ Digital Interfaces -----------------------------------------------*/

#if (STD_ON == IOHWAB_DIGITAL_API)

#if (STD_ON == DIO_CHANNELS_API)

/**
 * \brief      Performs a synchronous read of the specified MCAL DIO channel.
 * \param      ChannelId: ID of a specific MCAL DIO channel.
 * \return     Result of the specified MCAL DIO channel reading.
 */
IoHwAb_DigitalLevelType IoHwAb_DigitalGetChannel(IoHwAb_DigitalChannelType ChannelId)
{
   return (IoHwAb_DigitalLevelType) Dio_ReadChannel((Dio_ChannelType) ChannelId);
}

/**
 * \brief      Performs a synchronous write on the specified MCAL DIO channel.
 * \param      ChannelId: ID of a specific MCAL DIO channel.
 * \param      Level: Digital level to be applied on the specified MCAL DIO channel.
 * \return     -
 */
void IoHwAb_DigitalSetChannel(IoHwAb_DigitalChannelType Channeld, IoHwAb_DigitalLevelType Level)
{
   Dio_WriteChannel((Dio_ChannelType) Channeld, (Dio_LevelType) Level);
}

/**
 * \brief      Performs a synchronous toggle and returns the current logical value on the specified MCAL DIO channel.
 * \param      ChannelId: ID of a specific MCAL DIO channel.
 * \return     Current, after toggle, logical value (STD_HIGH or STD_LOW) on the specified individual output channel.
 */
IoHwAb_DigitalLevelType IoHwAb_DigitalFlipChannel(IoHwAb_DigitalChannelType ChannelId)
{
   return (IoHwAb_DigitalLevelType) Dio_FlipChannel((Dio_ChannelType) ChannelId);
}

#endif /* (STD_ON == DIO_CHANNELS_API) */

#if (STD_ON == DIO_CHANNEL_GROUPS_API)

/**
 * \brief      Performs a synchronous read of the current logical value of the specified MCAL DIO channel group.
 * \param      ChannelGroupIdPtr : ID of a specific MCAL DIO channel group.
 * \return     Current logical values (masked and shifted to the LSB) of the specified MCAL DIO channel group.
 */
IoHwAb_DigitalPortLevelType IoHwAb_DigitalGetChannelGroup(IoHwAb_DigitalChannelGroupType ChannelGroupIdPtr)
{
   return (IoHwAb_DigitalPortLevelType) Dio_ReadChannelGroup((Dio_ChannelGroupType *) ChannelGroupIdPtr);
}

/**
 * \brief      Performs a synchronous write to the current logical value of the specified MCAL DIO channel group.
 * \param      ChannelGroupIdPtr : ID of a specific MCAL DIO channel group.
 * \param      Level: Logical value to be applied on the specified MCAL DIO channel group.
 * \return     -.
 */
void IoHwAb_DigitalSetChannelGroup(IoHwAb_DigitalChannelGroupType ChannelGroupIdPtr, IoHwAb_DigitalPortLevelType Level)
{
   Dio_WriteChannelGroup((Dio_ChannelGroupType *) ChannelGroupIdPtr, (Dio_PortLevelType) Level);
}

#endif /* (STD_ON == DIO_CHANNEL_GROUPS_API) */

#if (STD_ON == DIO_PORTS_API)

/**
 * \brief      Performs a synchronous read of the specified MCAL DIO port.
 * \param      ChannelId: ID of a specific MCAL DIO port.
 * \return     Result of the specified MCAL DIO port reading.
 */
IoHwAb_DigitalPortLevelType IoHwAb_DigitalGetPort(IoHwAb_DigitalPortChannelType PortId)
{
   return (IoHwAb_DigitalPortLevelType) Dio_ReadPort((Dio_PortType) PortId);
}

/**
 * \brief      Performs a synchronous write on the specified MCAL DIO port.
 * \param      ChannelId: ID of a specific MCAL DIO port.
 * \param      Level: Full port value to be applied on the specified MCAL DIO port.
 * \return     -
 */
void IoHwAb_DigitalSetPort(IoHwAb_DigitalPortChannelType PortId, IoHwAb_DigitalPortLevelType Level)
{
   Dio_WritePort((Dio_PortType) PortId, (Dio_PortLevelType) Level);
}

#endif /* #if (STD_ON == DIO_PORTS_API) */

#endif /* #if (STD_ON == IOHWAB_DIGITAL_API) */

/*-------------------------------------------------- PWM Interfaces -------------------------------------------------*/

#if (STD_ON == IOHWAB_PWM_API)

/**
 * \brief      Performs a synchronous duty cycle update of the specified MCAL PWM channel.
 * \param      ChannelId: ID of a specific MCAL PWM channel.
 * \param      Duty: Duty cycle to be applied on the specified MCAL PWM channel.
 * \return     -
 */
void IoHwAb_PwmSetChannel(IoHwAb_PwmChannelType ChannelId, IoHwAb_PwmDutyType Duty)
{
   Pwm_SetDutyCycle((Pwm_ChannelType) ChannelId, (uint16) Duty);
}

#endif /* (STD_ON == IOHWAB_PWM_API) */

#endif /* (STD_ON == IOHWAB_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Disp7Seg.c
 *    \author     Maria-Alexandra Zolya
 *    \brief      Declares all the  configuration functions for the 7 Segments Display.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Disp7Seg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
typedef struct
{
   uint8 uc_DecValue;
   Disp7Seg_DotsType Disp7Seg_Dots;
} Disp7Seg_DataType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static Disp7Seg_DataType Disp7Seg_Data[2];
static Disp7Seg_DotsType Disp7Seg_Dots;
static uint8 Disp7Seg_Left;
static uint8 Disp7Seg_Right;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Disp7Seg_Left_Digit(uint8 Left_Digit)
{
   if (Left_Digit == 0)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);
   }
   else if (Left_Digit == 1)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);
   }
   else if (Left_Digit == 2)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 3)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 4)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 5)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 6)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 7)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);
   }
   else if (Left_Digit == 8)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 9)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   }
   else if (Left_Digit == 255)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);
   }
}
void Disp7Seg_Right_Digit(uint8 Right_Digit)
{
   if (Right_Digit == 0)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
   }
   else if (Right_Digit == 1)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
   }
   else if (Right_Digit == 2)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 3)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 4)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 5)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 6)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 7)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
   }
   else if (Right_Digit == 8)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 9)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   }
   else if (Right_Digit == 255)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
   }
}
void Disp7Seg_Init(void)
{
   Disp7Seg_Data[0].uc_DecValue = 0xFF;
   Disp7Seg_Data[0].Disp7Seg_Dots = DISP7SEG_BOTH_OFF;
   IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0x8000);

   Disp7Seg_Data[1].uc_DecValue = 0xFF;
   Disp7Seg_Data[1].Disp7Seg_Dots = DISP7SEG_BOTH_OFF;
   IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0x8000);

}

void Disp7Seg_MainFunction(void)
{
   Disp7Seg_Left_Digit(Disp7Seg_Left);
   Disp7Seg_Right_Digit(Disp7Seg_Right);
   if (Disp7Seg_Dots == DISP7SEG_BOTH_OFF)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (Disp7Seg_Dots == DISP7SEG_BOTH_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }
   else if (Disp7Seg_Dots == DISP7SEG_LEFT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (Disp7Seg_Dots == DISP7SEG_RIGHT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }
}
extern void Disp7Seg_SetState(uint8 uc_Id, uint8 uc_DecValue, Disp7Seg_DotsType t_Dots)
{
   if (uc_DecValue < 100)
   {
      if (uc_Id == DISP7SEG_INSTANCE_DISPLAY)
      {
         Disp7Seg_Right = uc_DecValue % 10;
         Disp7Seg_Left = uc_DecValue / 10;
         Disp7Seg_Dots = t_Dots;
      }
   }
   else if (uc_DecValue == 255)
   {
      Disp7Seg_Right = uc_DecValue;
      Disp7Seg_Left = uc_DecValue;
      Disp7Seg_Dots = t_Dots;
   }
}

/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Btn.c
 *    \author     Maria-Alexandra Zolya
 *    \brief      Declares all the  configuration functions for the button.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Btn.h"
#include "Btn_Core.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/** \brief  Defines the output values after processing the input values that can be read through the get function. */
//static Btn_PressStateType Btn_at_OutputValues[BTN_NUMBER_OF_INSTANCES];
//static Btn_PressStateType Btn_state;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
uint8 i;
uint8 uc_Id = 0;
static Btn_Specif Btn_Array[3];
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/** void Btn_Init(void) � initializes all the static and (internally) global variables so that if an immediate reading
 *  of a button is performed from a task, before the debounce time window elapses, to read out the not pressed state.
 */
void Btn_Init(void)
{
   Btn_Array[BTN_INSTANCE_LEFT].State = BTN_NOT_PRESSED;
   Btn_Array[BTN_INSTANCE_LEFT].Debounce_Counter = 0;
   Btn_Array[BTN_INSTANCE_LEFT].Btn_Pin = IOHWAB_DIGITAL_CHANNEL_BTN_LEFT;

   Btn_Array[BTN_INSTANCE_RIGHT].State = BTN_NOT_PRESSED;
   Btn_Array[BTN_INSTANCE_RIGHT].Debounce_Counter = 0;
   Btn_Array[BTN_INSTANCE_RIGHT].Btn_Pin = IOHWAB_DIGITAL_CHANNEL_BTN_RIGHT;

   Btn_Array[BTN_INSTANCE_HAZARD].State = BTN_NOT_PRESSED;
   Btn_Array[BTN_INSTANCE_HAZARD].Debounce_Counter = 0;
   Btn_Array[BTN_INSTANCE_HAZARD].Btn_Pin = IOHWAB_DIGITAL_CHANNEL_BTN_HAZARD;
}

/** void Btn_MainFunction(void) � reads out all the digital input channels that are attached to the buttons, applies a
 *  debounce algorithm and prepares the output states as pressed or not pressed depending on the digital active level
 *  of each button.
 */
void Btn_MainFunction(void)
{

   //  Btn_state = IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_CHANNEL_BTN_LEFT);

   for (i = 0; i < 3; i++)
   {
      if (Btn_Array[i].State == BTN_NOT_PRESSED)
      {
         // Btn_Array[i].Debounce_Counter = 0;
         if (STD_HIGH == IoHwAb_DigitalGetChannel(Btn_Array[i].Btn_Pin))
        {
            Btn_Array[i].Debounce_Counter++;
            if (Btn_Array[i].Debounce_Counter > 4)
            {
               Btn_Array[i].State = BTN_PRESSED;
               Btn_Array[i].Debounce_Counter = 0;
            }
         }
         else
         {
            Btn_Array[i].State = BTN_NOT_PRESSED;
            Btn_Array[i].Debounce_Counter = 0;
         }
      }
      else
      {
         if (STD_LOW == IoHwAb_DigitalGetChannel(Btn_Array[i].Btn_Pin))
         {
            Btn_Array[i].Debounce_Counter++;
            if (Btn_Array[i].Debounce_Counter > 4)
            {
               Btn_Array[i].State = BTN_NOT_PRESSED;
               Btn_Array[i].Debounce_Counter = 0;
            }

         }
         else
         {
            Btn_Array[i].State = BTN_PRESSED;
            Btn_Array[i].Debounce_Counter = 0;
         }
      }
   }
}

/**
 * \brief      Returns the current output state of the specified instance.
 * \param      uc_Id : specific ID of a instance.
 * \return     The current output value of the specified instance.
 */

Btn_PressStateType Btn_GetState(uint8 uc_Id)
{
//   uc_Id++;
//returnare stare variabila globala
   return Btn_Array[uc_Id].State;
   // return Btn_Array[1].State;
}


/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Tasks.c
 *    \author     Maria-Alexandra Zolya
 *    \brief      Implements all the OS tasks. Maps software component runnables to tasks.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Os.h"
#include "IoHwAb.h"
#include "Btn.h"
#include "PotMet.h"
#include "Led.h"
#include "Disp7Seg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Specifies the upper duty cycle limit to be used in the LEDs dimming. */
#define RTE_DIMMING_DUTY_UPPER_LIMIT           (0x2000UL)

#define CONFIG_LEFT_RIGHT_ON_OFF_PERIODS 250 ms
#define CONFIG_HAZARD_ON_OFF_PERIODS 500 ms
#define CONFIG_LEDS_INTENSITY 100
#define CONFIG_LEFT_RIGHT_NR_OF_FLASHES 4
#define INPUT_MIN_POT 30
#define INPUT_MAX_POT 9970
#define OUTPUT_MIN_POT 100
#define OUTPUT_MAX_POT 900
#define OUTPUT_MIN_POT3 10
#define OUTPUT_MAX_POT3 1000
#define OUTPUT_MIN_POT4 20
#define OUTPUT_MAX_POT4 60

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef enum
{
   RTE_STATE_MACHINE_IDLE,
   RTE_STATE_MACHINE_RIGHT_SIGNALING,
   RTE_STATE_MACHINE_LEFT_SIGNALING,
   RTE_STATE_MACHINE_HAZARD_SIGNALING,
   RTE_STATE_MACHINE_CONFIG_1_SIGNALING,
   RTE_STATE_MACHINE_CONFIG_2_SIGNALING,
   RTE_STATE_MACHINE_CONFIG_3_SIGNALING,
   RTE_STATE_MACHINE_CONFIG_4_SIGNALING,
} Rte_StateMachineType;

typedef enum
{
   RTE_STATE_MACHINE_NORMAL_SIGNALING,
   RTE_STATE_MACHINE_CONFIG_SIGNALING,
} Rte_StateType;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static uint8 counter = 0;
static uint16 counterH =0;
static uint8 counterForHazard = 0;
//static uint8 counterForHazardOff = 0;
//static uint8 counterForHazardDebounce = 0;
static uint8 counterFlashTime = 4;

static Rte_StateMachineType Rte_t_CurrentState = RTE_STATE_MACHINE_IDLE;
Btn_PressStateType Rte_gt_BtnLastRightPress = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_BtnLastLeftPress = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_BtnLastHazardPress = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_BtnLastHazardPressOff = BTN_PRESSED;

Btn_PressStateType t_LastBtnStateRight = BTN_NOT_PRESSED;
Btn_PressStateType t_LastBtnStateLeft = BTN_NOT_PRESSED;
Btn_PressStateType t_LastBtnStateHazard = BTN_NOT_PRESSED;

Btn_PressStateType Rte_gt_BtnLastConfigBtnLeftPress = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_BtnLastConfigBtnRightPress = BTN_NOT_PRESSED;

uint8 configStateFunctionCounter = 0;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
void Idle_Function()
{
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 255, DISP7SEG_BOTH_ON);
}
void Right_Turn_Signaling_Function()
{
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, counterFlashTime, DISP7SEG_RIGHT_ON);
   if (counter < 50)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 10000);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 10000);
   }
   else if (counter < 200)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   }
   counter++;
   if (counter >= 200)
   {
      counter = 0;
      counterFlashTime--;
   }
   if (counterFlashTime < 1)
   {
      Rte_t_CurrentState = RTE_STATE_MACHINE_IDLE;
      counterFlashTime = 4;
   }
}
void Left_Turn_Signaling_Function()
{
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, counterFlashTime, DISP7SEG_LEFT_ON);
   if (counter < 50)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 10000);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 10000);
   }
   else if (counter < 200)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   }
   counter++;
   if (counter >= 200)
   {
      counter = 0;
      counterFlashTime--;
   }
   if (counterFlashTime < 1)
   {
      Rte_t_CurrentState = RTE_STATE_MACHINE_IDLE;
      counterFlashTime = 4;
   }
}
void Hazard_Turn_Signaling_Function()
{
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);

   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 255, DISP7SEG_BOTH_OFF);
   if (counter < 50)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 10000);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 10000);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 10000);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 10000);
   }
   else if (counter < 200)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   }
   counter++;
   if (counter >= 200)
   {
      counter = 0;
   }
}
void NormalStateFunction()
{
   //BTN LEFT
   if (Rte_t_CurrentState != RTE_STATE_MACHINE_HAZARD_SIGNALING)
   {
      if (Rte_t_CurrentState == RTE_STATE_MACHINE_IDLE)
      {
         Idle_Function();
      }
      Btn_PressStateType t_BtnCurrentLeft;
      t_BtnCurrentLeft = Btn_GetState(BTN_INSTANCE_LEFT);
      if ((t_BtnCurrentLeft == BTN_NOT_PRESSED) && (Rte_gt_BtnLastLeftPress == BTN_PRESSED))
      {
         Rte_t_CurrentState = RTE_STATE_MACHINE_LEFT_SIGNALING;
         counter = 0;
         counterFlashTime = 4;
      }
      else
      {
      }
      if (Rte_t_CurrentState == RTE_STATE_MACHINE_LEFT_SIGNALING)
      {
         Left_Turn_Signaling_Function();
      }
      else
      {
      }
      Rte_gt_BtnLastLeftPress = t_BtnCurrentLeft;

      //BTN RIGHT
      Btn_PressStateType t_BtnCurrentRight;
      t_BtnCurrentRight = Btn_GetState(BTN_INSTANCE_RIGHT);
      if ((t_BtnCurrentRight == BTN_NOT_PRESSED) && (Rte_gt_BtnLastRightPress == BTN_PRESSED))
      {
         Rte_t_CurrentState = RTE_STATE_MACHINE_RIGHT_SIGNALING;
         counter = 0;
         counterFlashTime = 4;
      }
      else
      {
      }
      if (Rte_t_CurrentState == RTE_STATE_MACHINE_RIGHT_SIGNALING)
      {
         Right_Turn_Signaling_Function();
      }
      else
      {
      }
      Rte_gt_BtnLastRightPress = t_BtnCurrentRight;
   }
   Btn_PressStateType t_BtnCurrentHazard;
   // HAZARD BUTTON
   t_BtnCurrentHazard = Btn_GetState(BTN_INSTANCE_HAZARD);

   if (t_BtnCurrentHazard == BTN_PRESSED)
   {
      if (counterForHazard <= 40U)
      {
         counterForHazard++;
      }
   }
   else
   {
      counterForHazard = 0U;
   }

   if (counterForHazard == 40U)
   {
      if (Rte_t_CurrentState == RTE_STATE_MACHINE_HAZARD_SIGNALING)
      {
         Rte_t_CurrentState = RTE_STATE_MACHINE_IDLE;
      }
      else
      {
         Rte_t_CurrentState = RTE_STATE_MACHINE_HAZARD_SIGNALING;
      }
      counter = 0U;
   }
   if (Rte_t_CurrentState == RTE_STATE_MACHINE_HAZARD_SIGNALING)
   {
      Hazard_Turn_Signaling_Function();
   }
   else
   {
   }
   Rte_gt_BtnLastHazardPress = t_BtnCurrentHazard;

}
static uint16 interpolarFunctionConfig12(uint16 INPUT)
{
   uint16 OUTPUT = (uint32) (INPUT - INPUT_MIN_POT) * (OUTPUT_MAX_POT - OUTPUT_MIN_POT) / (INPUT_MAX_POT -
   INPUT_MIN_POT) + OUTPUT_MIN_POT;
   return OUTPUT;
}
static uint16 interpolarFunctionConfig3(uint16 INPUT)
{
   uint16 OUTPUT = (uint32) (INPUT - INPUT_MIN_POT) * (OUTPUT_MAX_POT3 - OUTPUT_MIN_POT3) / (INPUT_MAX_POT -
   INPUT_MIN_POT) + OUTPUT_MIN_POT3;
   return OUTPUT;
}
static uint16 interpolarFunctionConfig4(uint16 INPUT)
{
   uint16 OUTPUT = (uint32) (INPUT - INPUT_MIN_POT) * (OUTPUT_MAX_POT4 - OUTPUT_MIN_POT4) / (INPUT_MAX_POT -
   INPUT_MIN_POT) + OUTPUT_MIN_POT4;
   return OUTPUT;
}
//volatile PotMet_PositionType cici;
uint16 potValInt;
void Config1Function()
{
   uint16 potVal;
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 10000);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);

   potValInt = PotMet_GetState(POTMET_INSTANCE_POTENTIOMETER);
   potVal = interpolarFunctionConfig12(potValInt);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, potVal / 10, DISP7SEG_BOTH_OFF);
}
void Config2Function()
{
   uint16 potVal;
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 10000);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);

   potValInt = PotMet_GetState(POTMET_INSTANCE_POTENTIOMETER);
   potVal = interpolarFunctionConfig12(potValInt);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, potVal / 10, DISP7SEG_BOTH_OFF);
}
void Config3Function()
{
   uint16 potVal;
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 10000);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);

   potValInt = PotMet_GetState(POTMET_INSTANCE_POTENTIOMETER);
   potVal = interpolarFunctionConfig3(potValInt);
   if (potVal / 10 < 100)
   {
      Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, potVal / 10, DISP7SEG_BOTH_OFF);
//      Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 10, DISP7SEG_BOTH_ON);
   }
   if (potVal / 10 == 100)
   {
      Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 10, DISP7SEG_BOTH_ON);
   }
}
void Config4Function()
{
   uint16 potVal;
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 10000);

   potValInt = PotMet_GetState(POTMET_INSTANCE_POTENTIOMETER);
   potVal = interpolarFunctionConfig4(potValInt);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, potVal / 10, DISP7SEG_BOTH_OFF);
}
void ConfigStateFunction()
{
   Btn_PressStateType t_CurrentLeft;
     t_CurrentLeft = Btn_GetState(BTN_INSTANCE_LEFT);
     if ((t_LastBtnStateLeft == BTN_NOT_PRESSED) && (t_CurrentLeft == BTN_PRESSED))
     {
        if (Rte_t_CurrentState == RTE_STATE_MACHINE_CONFIG_1_SIGNALING)
        {
           Rte_t_CurrentState = RTE_STATE_MACHINE_CONFIG_4_SIGNALING;
        }
        else
        {
           Rte_t_CurrentState--;
        }
     }
     else
     {

     }
     Btn_PressStateType t_CurrentRight;
     t_CurrentRight = Btn_GetState(BTN_INSTANCE_RIGHT);
     if ((t_LastBtnStateRight == BTN_NOT_PRESSED) && (t_CurrentRight == BTN_PRESSED))
     {
        if (Rte_t_CurrentState == RTE_STATE_MACHINE_CONFIG_4_SIGNALING)
        {
           Rte_t_CurrentState = RTE_STATE_MACHINE_CONFIG_1_SIGNALING;
        }
        else
        {
           Rte_t_CurrentState++;
        }
     }
     else
     {

     }
     t_LastBtnStateLeft = t_CurrentLeft;
     t_LastBtnStateRight = t_CurrentRight;
     switch (Rte_t_CurrentState)
     {
        case RTE_STATE_MACHINE_IDLE:
        {

           break;
        }
        case RTE_STATE_MACHINE_LEFT_SIGNALING:
        {

           break;
        }
        case RTE_STATE_MACHINE_RIGHT_SIGNALING:
        {

           break;
        }
        case RTE_STATE_MACHINE_HAZARD_SIGNALING:
        {

           break;
        }
        case RTE_STATE_MACHINE_CONFIG_1_SIGNALING:
        {
           Config1Function();
           break;
        }
        case RTE_STATE_MACHINE_CONFIG_2_SIGNALING:
        {
           Config2Function();
           break;
        }
        case RTE_STATE_MACHINE_CONFIG_3_SIGNALING:
        {
           Config3Function();
           break;
        }
        case RTE_STATE_MACHINE_CONFIG_4_SIGNALING:
        {
           Config4Function();
           break;
        }
     }

}
void Application()
{

   Btn_PressStateType t_CurrentLeft;
   Btn_PressStateType t_CurrentRight;
   t_CurrentLeft = Btn_GetState(BTN_INSTANCE_LEFT);
   t_CurrentRight = Btn_GetState(BTN_INSTANCE_RIGHT);

   if ((t_CurrentLeft == BTN_PRESSED) && (t_CurrentRight == BTN_PRESSED))
   {
      if (counterH <= 400)
      {
         counterH++;
      }
   }
   else
   {
      if (counterH > 400)
      {
         if ((t_CurrentLeft == BTN_NOT_PRESSED) && (t_CurrentRight == BTN_NOT_PRESSED))
         {
            counterH = 0;
         }
      }
      else
      {
         counterH = 0;
      }
   }

   if (counterH == 400)
   {
      if (Rte_t_CurrentState <= RTE_STATE_MACHINE_HAZARD_SIGNALING)
      {
         Rte_t_CurrentState = RTE_STATE_MACHINE_CONFIG_1_SIGNALING;
      }
      else
      {
         Rte_t_CurrentState = RTE_STATE_MACHINE_IDLE;
      }
   }
   else
   {

   }

   if (Rte_t_CurrentState <= RTE_STATE_MACHINE_HAZARD_SIGNALING)
   {
      NormalStateFunction();
   }
   else
   {
      ConfigStateFunction();
   }

}
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes RTE and all the SWCs.
 * \param      -
 * \return     -
 */
TASK(OS_INIT_TASK)
{
   Btn_Init();
   PotMet_Init();
   Led_Init();
   Disp7Seg_Init();
   Idle_Function();
}

/**
 * \brief      Application specific 500 us periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_FAST_TASK)
{
}

/**
 * \brief      Application specific 5 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_5MS_TASK)
{
//   Btn_PressStateType t_BtnCurrentConfigBtnLeft;
//   Btn_PressStateType t_BtnCurrentConfigBtnRight;
//   t_BtnCurrentConfigBtnLeft = Btn_GetState(BTN_INSTANCE_LEFT);
//   t_BtnCurrentConfigBtnRight = Btn_GetState(BTN_INSTANCE_RIGHT);
//
//   if ((t_BtnCurrentConfigBtnLeft == BTN_PRESSED) && (t_BtnCurrentConfigBtnRight == BTN_PRESSED) &&
//      configStateFunctionCounter == 0)
//   {
//      Rte_t_CurrentStateType = RTE_STATE_MACHINE_NORMAL_SIGNALING;
//   }
//   else if ((t_BtnCurrentConfigBtnLeft == BTN_PRESSED) && (t_BtnCurrentConfigBtnRight == BTN_PRESSED) &&
//      configStateFunctionCounter == 1)
//   {
//      Rte_t_CurrentStateType = RTE_STATE_MACHINE_CONFIG_SIGNALING;
//   }
//
//   switch (Rte_t_CurrentStateType)
//   {
//      case RTE_STATE_MACHINE_NORMAL_SIGNALING:
//      {
//        // Idle_Function();
//         NormalStateFunction();
//         configStateFunctionCounter = 1;
//         break;
//      }
//      case RTE_STATE_MACHINE_CONFIG_SIGNALING:
//      {
//        // Idle_Function();
//         ConfigStateFunction();
//         configStateFunctionCounter = 0;
//         break;
//      }
//   }

   Btn_MainFunction();
   PotMet_MainFunction();
   Led_MainFunction();
   Disp7Seg_MainFunction();
   Application();

}
TASK(OS_10MS_TASK)
{

}

/**
 * \brief      Application specific 20 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_20MS_TASK)
{

}

/**
 * \brief      Application specific 100 ms periodicity task.
 * \param     -
 * \return    -
 */

TASK(OS_100MS_TASK)
{

}

/**
 * \brief      Application background task.
 * \param      -
 * \return     -
 */
TASK(OS_BACKGROUND_TASK)
{
}

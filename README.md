# Framework_Internship

Contains a basic AUTOSAR implementation of the ECU manager, MCAL drivers, ECU abstraction, Operating System, RTE and a series of source code templates. It also provides example SWCs and an RTE configuration.

This framework is meant for the Embedded Internship projects which focuses mainly on developing application modules.